<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Salir si se accede directamente
}

// Función para crear la tabla en la base de datos
function kmi_create_database_table() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'kmi_maps';
    $charset_collate = $wpdb->get_charset_collate();

    // Asegurar que la variable $wpdb->prefix tenga un valor
    if ( ! isset( $wpdb->prefix ) ) {
        $wpdb->prefix = $wpdb->base_prefix;
    }

    $sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        map_name varchar(255) NOT NULL,
        map_description text NOT NULL,
        geojson_file varchar(255) NOT NULL,
        zip_file varchar(255) DEFAULT '',
        shortcode text NOT NULL,
        fields text NOT NULL,
        search_field varchar(255) DEFAULT '',
        group_field varchar(255) DEFAULT '',
        date_created datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

// Funciones para interactuar con la base de datos
function kmi_insert_map( $data ) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'kmi_maps';

    $wpdb->insert( $table_name, $data );
    return $wpdb->insert_id;
}

function kmi_get_maps( $search = '' ) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'kmi_maps';

    if ( ! empty( $search ) ) {
        $sql = $wpdb->prepare( "SELECT * FROM $table_name WHERE map_name LIKE %s ORDER BY date_created DESC", '%' . $wpdb->esc_like( $search ) . '%' );
    } else {
        $sql = "SELECT * FROM $table_name ORDER BY date_created DESC";
    }

    return $wpdb->get_results( $sql );
}

function kmi_get_map( $id ) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'kmi_maps';

    $sql = $wpdb->prepare( "SELECT * FROM $table_name WHERE id = %d", $id );
    return $wpdb->get_row( $sql );
}

function kmi_update_map( $id, $data ) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'kmi_maps';

    return $wpdb->update( $table_name, $data, array( 'id' => $id ) );
}

function kmi_delete_map( $id ) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'kmi_maps';

    return $wpdb->delete( $table_name, array( 'id' => $id ) );
}
