<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Salir si se accede directamente
}

function kmi_maps_list_page() {
    if ( ! current_user_can('manage_options') ) {
        return;
    }

    if ( isset($_GET['action']) && $_GET['action'] == 'delete' && isset($_GET['map_id']) && check_admin_referer('delete_map_' . $_GET['map_id']) ) {
        $map_id = intval($_GET['map_id']);
        kmi_delete_map($map_id);
        echo '<div class="notice notice-success is-dismissible"><p>' . esc_html__('Mapa eliminado correctamente.', 'kobo-maps-integration') . '</p></div>';
    }

    $search = isset($_GET['s']) ? sanitize_text_field($_GET['s']) : '';
    $maps = kmi_get_maps($search);
    ?>
    <div class="wrap">
        <h1><?php esc_html_e('Mapas', 'kobo-maps-integration'); ?> 
            <a href="<?php echo admin_url('admin.php?page=kmi_add_new_map'); ?>" class="page-title-action">
                <?php esc_html_e('Añadir Nuevo Mapa', 'kobo-maps-integration'); ?>
            </a>
        </h1>
        <form method="get">
            <input type="hidden" name="page" value="kobo_maps">
            <p class="search-box">
                <label class="screen-reader-text" for="map-search-input"><?php esc_html_e('Buscar Mapas:', 'kobo-maps-integration'); ?></label>
                <input type="search" id="map-search-input" name="s" value="<?php echo isset($_GET['s']) ? esc_attr($_GET['s']) : ''; ?>">
                <input type="submit" id="search-submit" class="button" value="<?php esc_attr_e('Buscar Mapas', 'kobo-maps-integration'); ?>">
            </p>
        </form>
        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th><?php esc_html_e('ID', 'kobo-maps-integration'); ?></th>
                    <th><?php esc_html_e('Nombre del Mapa', 'kobo-maps-integration'); ?></th>
                    <th><?php esc_html_e('Shortcode', 'kobo-maps-integration'); ?></th>
                    <th><?php esc_html_e('Acciones', 'kobo-maps-integration'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if ($maps) : ?>
                    <?php foreach ($maps as $map) : ?>
                    <tr>
                        <td><?php echo esc_html($map->id); ?></td>
                        <td><?php echo esc_html($map->map_name); ?></td>
                        <td>
                            <button class="button kmi-copy-shortcode" data-shortcode="<?php echo esc_attr($map->shortcode); ?>">
                                <?php esc_html_e('Copiar Shortcode', 'kobo-maps-integration'); ?>
                            </button>
                        </td>
                        <td>
                            <a href="<?php echo admin_url('admin.php?page=kmi_add_new_map&action=edit&map_id=' . $map->id); ?>">
                                <?php esc_html_e('Editar', 'kobo-maps-integration'); ?>
                            </a> |
                            <a href="<?php echo wp_nonce_url(admin_url('admin.php?page=kobo_maps&action=delete&map_id=' . $map->id), 'delete_map_' . $map->id); ?>" 
                               onclick="return confirm('<?php esc_html_e('¿Estás seguro de que deseas eliminar este mapa?', 'kobo-maps-integration'); ?>');">
                                <?php esc_html_e('Eliminar', 'kobo-maps-integration'); ?>
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="4"><?php esc_html_e('No se encontraron mapas.', 'kobo-maps-integration'); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <script>
        jQuery(document).ready(function($) {
            $('.kmi-copy-shortcode').on('click', function(e) {
                e.preventDefault();
                var shortcode = $(this).data('shortcode');
                var $temp = $("<textarea>");
                $("body").append($temp);
                $temp.val(shortcode).select();
                document.execCommand("copy");
                $temp.remove();
                alert('<?php esc_html_e('Shortcode copiado al portapapeles.', 'kobo-maps-integration'); ?>');
            });
        });
        </script>
    </div>
    <?php
}

function kmi_add_new_map_page() {
    if ( ! current_user_can('manage_options') ) {
        return;
    }

    $is_edit = false;
    $map = null;
    if ( isset($_GET['action']) && $_GET['action'] == 'edit' && isset($_GET['map_id']) ) {
        $is_edit = true;
        $map_id = intval($_GET['map_id']);
        $map = kmi_get_map($map_id);
    }

    $errors = array();
    $success = '';

    if ( isset($_POST['kmi_save_map']) || isset($_POST['kmi_save_map_step2']) ) {
        $nonce_action = $is_edit ? 'kmi_edit_map' : 'kmi_save_map';
        check_admin_referer($nonce_action);

        $map_name = sanitize_text_field($_POST['map_name']);
        $map_description = sanitize_textarea_field($_POST['map_description']);

        $upload_dir = wp_upload_dir();
        $kobo_maps_dir = $upload_dir['basedir'] . '/kobo-maps/' . sanitize_file_name($map_name);
        if ( ! file_exists($kobo_maps_dir) ) {
            wp_mkdir_p($kobo_maps_dir);
        }

        if ( isset($_POST['kmi_save_map']) ) {
            // Paso 1: Procesar archivos
            if ( isset($_FILES['geojson_file']) && $_FILES['geojson_file']['size'] > 0 ) {
                $geojson_file = $_FILES['geojson_file'];
                $geojson_path = $kobo_maps_dir . '/' . sanitize_file_name($geojson_file['name']);
                move_uploaded_file($geojson_file['tmp_name'], $geojson_path);

                $fixed_geojson_path = kmi_process_geojson_file($geojson_path);
                if ( is_wp_error($fixed_geojson_path) ) {
                    $errors[] = $fixed_geojson_path->get_error_message();
                }
            } else {
                if ( $is_edit && $map ) {
                    $fixed_geojson_path = $map->geojson_file;
                } else {
                    $errors[] = __('Por favor, sube un archivo GeoJSON.', 'kobo-maps-integration');
                    $fixed_geojson_path = '';
                }
            }

            if ( isset($_FILES['zip_file']) && $_FILES['zip_file']['size'] > 0 ) {
                $zip_file = $_FILES['zip_file'];
                $zip_path = $kobo_maps_dir . '/' . sanitize_file_name($zip_file['name']);
                move_uploaded_file($zip_file['tmp_name'], $zip_path);

                $attachments_dir = $kobo_maps_dir . '/attachments';
                $zip_result = kmi_process_zip_file($zip_path, $attachments_dir);
                if ( is_wp_error($zip_result) ) {
                    $errors[] = $zip_result->get_error_message();
                } else {
                    kmi_organize_attachments($attachments_dir, $attachments_dir);
                }
            } else {
                if ( $is_edit && $map ) {
                    $zip_path = $map->zip_file;
                } else {
                    $zip_path = '';
                }
            }

            if ( empty($errors) ) {
                // Obtener campos disponibles
                $available_fields = array();
                if ( ! empty($fixed_geojson_path) && file_exists($fixed_geojson_path) ) {
                    $geojson_data = json_decode(file_get_contents($fixed_geojson_path), true);
                    if ( isset($geojson_data['features'][0]['properties']) ) {
                        $available_fields = array_keys($geojson_data['features'][0]['properties']);
                    }
                }

                // Mostrar paso 2
                ?>
                <div class="wrap">
                    <h1><?php echo esc_html__('Configurar Mapa', 'kobo-maps-integration'); ?></h1>
                    <?php
                    if ( ! empty($errors) ) {
                        foreach ($errors as $error) {
                            echo '<div class="notice notice-error is-dismissible"><p>' . esc_html($error) . '</p></div>';
                        }
                    }
                    if ( empty($available_fields) ) {
                        echo '<div class="notice notice-warning"><p>' . esc_html__('No se encontraron campos en el GeoJSON. Puedes continuar sin seleccionar campos.', 'kobo-maps-integration') . '</p></div>';
                    }
                    ?>
                    <form method="post">
                        <?php wp_nonce_field($nonce_action); ?>
                        <input type="hidden" name="map_name" value="<?php echo esc_attr($map_name); ?>">
                        <input type="hidden" name="map_description" value="<?php echo esc_attr($map_description); ?>">
                        <input type="hidden" name="geojson_file_path" value="<?php echo esc_attr($fixed_geojson_path); ?>">
                        <input type="hidden" name="zip_file_path" value="<?php echo esc_attr($zip_path); ?>">
                        <table class="form-table">
                            <?php if ( ! empty($available_fields) ) : ?>
                            <tr>
                                <th><?php esc_html_e('Seleccionar Campos para Popups', 'kobo-maps-integration'); ?></th>
                                <td>
                                    <?php foreach ($available_fields as $field) : ?>
                                    <label><input type="checkbox" name="fields[]" value="<?php echo esc_attr($field); ?>"> <?php echo esc_html($field); ?></label><br>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="search_field"><?php esc_html_e('Campo para Búsqueda', 'kobo-maps-integration'); ?></label></th>
                                <td>
                                    <select name="search_field" id="search_field">
                                        <option value=""><?php esc_html_e('Seleccionar campo', 'kobo-maps-integration'); ?></option>
                                        <?php foreach ($available_fields as $field) : ?>
                                        <option value="<?php echo esc_attr($field); ?>"><?php echo esc_html($field); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="group_field"><?php esc_html_e('Campo para Agrupación', 'kobo-maps-integration'); ?></label></th>
                                <td>
                                    <select name="group_field" id="group_field">
                                        <option value=""><?php esc_html_e('Seleccionar campo', 'kobo-maps-integration'); ?></option>
                                        <?php foreach ($available_fields as $field) : ?>
                                        <option value="<?php echo esc_attr($field); ?>"><?php echo esc_html($field); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <?php endif; ?>
                        </table>
                        <?php
                        $button_text = $is_edit ? esc_html__('Actualizar Mapa', 'kobo-maps-integration') : esc_html__('Guardar Mapa', 'kobo-maps-integration');
                        submit_button($button_text, 'primary', 'kmi_save_map_step2');
                        ?>
                    </form>
                </div>
                <?php
                return; 
            } else {
                foreach ($errors as $error) {
                    echo '<div class="notice notice-error is-dismissible"><p>' . esc_html($error) . '</p></div>';
                }
            }
        } elseif ( isset($_POST['kmi_save_map_step2']) ) {
            // Paso 2: Guardar mapa y generar shortcode

            // Procesar archivos subidos
            $map_name = sanitize_text_field( $_POST['map_name'] );
            $map_description = sanitize_textarea_field( $_POST['map_description'] );

            $upload_dir = wp_upload_dir();
            $kobo_maps_dir = $upload_dir['basedir'] . '/kobo-maps/' . sanitize_file_name( $map_name );
            if ( ! file_exists( $kobo_maps_dir ) ) {
                wp_mkdir_p( $kobo_maps_dir );
            }

            // Procesar archivo GeoJSON
            if ( isset( $_FILES['geojson_file'] ) && $_FILES['geojson_file']['size'] > 0 ) {
                $geojson_file = $_FILES['geojson_file'];
                $geojson_path = $kobo_maps_dir . '/' . sanitize_file_name( $geojson_file['name'] );
                move_uploaded_file( $geojson_file['tmp_name'], $geojson_path );

                // Validar y corregir GeoJSON
                $fixed_geojson_path = kmi_process_geojson_file( $geojson_path );
                if ( is_wp_error( $fixed_geojson_path ) ) {
                    $errors[] = $fixed_geojson_path->get_error_message();
                }
            } else {
                // Usar el archivo existente
                $fixed_geojson_path = isset( $_POST['geojson_file_path'] ) ? sanitize_text_field( $_POST['geojson_file_path'] ) : '';
            }

            // Procesar archivo ZIP
            if ( isset( $_FILES['zip_file'] ) && $_FILES['zip_file']['size'] > 0 ) {
                $zip_file = $_FILES['zip_file'];
                $zip_path = $kobo_maps_dir . '/' . sanitize_file_name( $zip_file['name'] );
                move_uploaded_file( $zip_file['tmp_name'], $zip_path );

                // Extraer y organizar archivos multimedia
                $attachments_dir = $kobo_maps_dir . '/attachments';
                $zip_result = kmi_process_zip_file( $zip_path, $attachments_dir );
                if ( is_wp_error( $zip_result ) ) {
                    $errors[] = $zip_result->get_error_message();
                } else {
                    kmi_organize_attachments( $attachments_dir, $attachments_dir );
                }
            } else {
                // Usar el archivo existente
                $zip_path = isset( $_POST['zip_file_path'] ) ? sanitize_text_field( $_POST['zip_file_path'] ) : '';
            }

            // Obtener los campos seleccionados y otros datos
            $fields = isset($_POST['fields']) ? array_map('sanitize_text_field', $_POST['fields']) : array();
            $search_field = isset($_POST['search_field']) ? sanitize_text_field($_POST['search_field']) : '';
            $group_field = isset($_POST['group_field']) ? sanitize_text_field($_POST['group_field']) : '';

            if ( empty($fixed_geojson_path) ) {
                $errors[] = __('El archivo GeoJSON no está especificado.', 'kobo-maps-integration');
            }

            if ( empty($errors) ) {
                $geojson_url = content_url('uploads/kobo-maps/' . sanitize_file_name($map_name) . '/' . basename($fixed_geojson_path));
                $shortcode = kmi_generate_leaflet_shortcode(array(
                    'geojson_url'        => $geojson_url,
                    'fields'             => $fields,
                    'search_field'       => $search_field,
                    'group_field'        => $group_field,
                    'geojson_file_path'  => $fixed_geojson_path,
                    'map_name'           => $map_name,
                ));

                $map_data = array(
                    'map_name'        => $map_name,
                    'map_description' => $map_description,
                    'geojson_file'    => $fixed_geojson_path,
                    'zip_file'        => $zip_path,
                    'shortcode'       => $shortcode,
                    'fields'          => maybe_serialize($fields),
                    'search_field'    => $search_field,
                    'group_field'     => $group_field,
                );

                if ( $is_edit && $map ) {
                    kmi_update_map($map->id, $map_data);
                    echo '<div class="notice notice-success is-dismissible"><p>' . esc_html__('Mapa actualizado correctamente.', 'kobo-maps-integration') . '</p></div>';
                    echo '<div class="notice notice-info is-dismissible">
                        <p>' . esc_html__('Shortcode generado:', 'kobo-maps-integration') . '</p>
                        <textarea id="kmi-generated-shortcode" rows="18" style="width:100%;">' . esc_textarea($shortcode) . '</textarea>
                        <button class="button" onclick="kmiCopyShortcode()">' . esc_html__('Copiar Shortcode', 'kobo-maps-integration') . '</button>
                    </div>';
                } else {
                    $map_id = kmi_insert_map($map_data);
                    echo '<div class="notice notice-success is-dismissible"><p>' . esc_html__('Mapa creado correctamente.', 'kobo-maps-integration') . '</p></div>';
                    // Botón para editar el mapa creado
                    echo '<a href="' . admin_url( 'admin.php?page=kmi_add_new_map&action=edit&map_id=' . $map_id ) . '" class="button button-primary">' . esc_html__( 'Editar Mapa', 'kobo-maps-integration' ) . '</a>';
                }
                ?>
                <script>
                function kmiCopyShortcode() {
                    var copyText = document.getElementById("kmi-generated-shortcode");
                    copyText.select();
                    document.execCommand("copy");
                    alert('<?php esc_html_e('Shortcode copiado al portapapeles.', 'kobo-maps-integration'); ?>');
                }
                </script>
                <?php
                echo '<p><a href="' . admin_url('admin.php?page=kobo_maps') . '" class="button">' . esc_html__('Volver a la lista de mapas', 'kobo-maps-integration') . '</a></p>';
                return; 
            } else {
                foreach ($errors as $error) {
                    echo '<div class="notice notice-error is-dismissible"><p>' . esc_html($error) . '</p></div>';
                }
            }
        }
    }

    if ( $is_edit && $map ) {
        $map_name = $map->map_name;
        $map_description = $map->map_description;
        $fixed_geojson_path = $map->geojson_file;
        $zip_path = $map->zip_file;
        $fields = maybe_unserialize($map->fields);
        $search_field = $map->search_field;
        $group_field = $map->group_field;
    } else {
        $map_name = '';
        $map_description = '';
        $fields = array();
        $search_field = '';
        $group_field = '';
    }

    $available_fields = array();
    if ( isset($fixed_geojson_path) && ! empty($fixed_geojson_path) && file_exists($fixed_geojson_path) ) {
        $geojson_data = json_decode(file_get_contents($fixed_geojson_path), true);
        if ( isset($geojson_data['features'][0]['properties']) ) {
            $available_fields = array_keys($geojson_data['features'][0]['properties']);
        }
    }

    ?>
    <div class="wrap">
        <h1><?php echo $is_edit ? esc_html__('Editar Mapa', 'kobo-maps-integration') : esc_html__('Añadir Nuevo Mapa', 'kobo-maps-integration'); ?></h1>
        <?php
        if ( ! empty($errors) ) {
            foreach ($errors as $error) {
                echo '<div class="notice notice-error is-dismissible"><p>' . esc_html($error) . '</p></div>';
            }
        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <?php
            $nonce_action = $is_edit ? 'kmi_edit_map' : 'kmi_save_map';
            wp_nonce_field($nonce_action);
            ?>
            <table class="form-table">
                <tr>
                    <th><label for="map_name"><?php esc_html_e('Nombre del Mapa', 'kobo-maps-integration'); ?></label></th>
                    <td><input type="text" name="map_name" id="map_name" value="<?php echo esc_attr($map_name); ?>" class="regular-text" required></td>
                </tr>
                <tr>
                    <th><label for="map_description"><?php esc_html_e('Descripción del Mapa', 'kobo-maps-integration'); ?></label></th>
                    <td><textarea name="map_description" id="map_description" class="large-text"><?php echo esc_textarea($map_description); ?></textarea></td>
                </tr>
                <tr>
                    <th><label for="geojson_file"><?php esc_html_e('Archivo GeoJSON', 'kobo-maps-integration'); ?></label></th>
                    <td>
                        <input type="file" name="geojson_file" id="geojson_file" accept=".geojson,application/geojson">
                        <?php if ( $is_edit && ! empty($fixed_geojson_path) && file_exists($fixed_geojson_path) ) : ?>
                            <p><?php esc_html_e('Archivo actual:', 'kobo-maps-integration'); ?> <?php echo esc_html(basename($fixed_geojson_path)); ?></p>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th><label for="zip_file"><?php esc_html_e('Archivo ZIP de Multimedia', 'kobo-maps-integration'); ?></label></th>
                    <td>
                        <input type="file" name="zip_file" id="zip_file" accept=".zip,application/zip">
                        <?php if ( $is_edit && ! empty($zip_path) && file_exists($zip_path) ) : ?>
                            <p><?php esc_html_e('Archivo actual:', 'kobo-maps-integration'); ?> <?php echo esc_html(basename($zip_path)); ?></p>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php if ( ! empty($available_fields) ) : ?>
                <tr>
                    <th><?php esc_html_e('Seleccionar Campos para Popups', 'kobo-maps-integration'); ?></th>
                    <td>
                        <?php foreach ($available_fields as $field) : ?>
                        <label><input type="checkbox" name="fields[]" value="<?php echo esc_attr($field); ?>" <?php checked(in_array($field, $fields)); ?>> <?php echo esc_html($field); ?></label><br>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th><label for="search_field"><?php esc_html_e('Campo para Búsqueda', 'kobo-maps-integration'); ?></label></th>
                    <td>
                        <select name="search_field" id="search_field">
                            <option value=""><?php esc_html_e('Seleccionar campo', 'kobo-maps-integration'); ?></option>
                            <?php foreach ($available_fields as $field) : ?>
                            <option value="<?php echo esc_attr($field); ?>" <?php selected($search_field, $field); ?>><?php echo esc_html($field); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label for="group_field"><?php esc_html_e('Campo para Agrupación', 'kobo-maps-integration'); ?></label></th>
                    <td>
                        <select name="group_field" id="group_field">
                            <option value=""><?php esc_html_e('Seleccionar campo', 'kobo-maps-integration'); ?></option>
                            <?php foreach ($available_fields as $field) : ?>
                            <option value="<?php echo esc_attr($field); ?>" <?php selected($group_field, $field); ?>><?php echo esc_html($field); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <?php else : ?>
                <!-- Sin campos, usuario puede continuar -->
                <?php endif; ?>
            </table>
            <?php
            if ( isset($fixed_geojson_path) ) {
                echo '<input type="hidden" name="geojson_file_path" value="' . esc_attr($fixed_geojson_path) . '">';
            }
            if ( isset($zip_path) ) {
                echo '<input type="hidden" name="zip_file_path" value="' . esc_attr($zip_path) . '">';
            }

            $button_text = $is_edit ? esc_html__('Actualizar Mapa', 'kobo-maps-integration') : esc_html__('Guardar Mapa', 'kobo-maps-integration');
            submit_button($button_text, 'primary', 'kmi_save_map_step2');
            ?>
        </form>
    </div>
    <?php
}
