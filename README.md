# Kobo Maps Integration

## Descripción

**Kobo Maps Integration** es un plugin de WordPress que permite a los administradores crear, gestionar y mostrar mapas interactivos basados en archivos GeoJSON y archivos multimedia (imágenes, audio, video) empaquetados en ZIP. Utiliza el plugin [Leaflet Map](https://wordpress.org/plugins/leaflet-map/) para renderizar los mapas y proporciona shortcodes personalizables para integrarlos fácilmente en tus páginas y entradas.

### Características Principales

- **Creación de Mapas:** Sube archivos GeoJSON y ZIP para crear mapas interactivos.
- **Gestión de Archivos Multimedia:** Organiza imágenes, audio y video asociados a los puntos del mapa.
- **Configuración de Popups:** Selecciona qué campos del GeoJSON se mostrarán en los popups del mapa.
- **Búsqueda y Agrupación:** Configura campos para habilitar la búsqueda y la agrupación de marcadores en el mapa.
- **Shortcodes Personalizables:** Genera shortcodes automáticamente para insertar mapas en cualquier parte de tu sitio.
- **Interfaz de Administración Intuitiva:** Gestiona fácilmente tus mapas desde el panel de administración de WordPress.

## Instalación

### Requisitos

- **WordPress 5.0 o superior**
- **Plugin Leaflet Map** instalado y activado. Puedes descargarlo desde [aquí](https://wordpress.org/plugins/leaflet-map/).
- **Plugin Leaflet Map Extensions** instalado y activado. Puedes descargarlo desde [aquí](https://wordpress.org/plugins/extensions-leaflet-map/).

### Pasos de Instalación

1. **Descargar el Plugin:**
   - Descarga el archivo ZIP de `kobo-maps-integration` desde tu repositorio o desde donde lo tengas disponible.

2. **Subir el Plugin a WordPress:**
   - Ve al panel de administración de WordPress.
   - Navega a `Plugins` > `Añadir nuevo`.
   - Haz clic en `Subir plugin`.
   - Selecciona el archivo ZIP descargado y haz clic en `Instalar ahora`.

3. **Activar el Plugin:**
   - Una vez instalado, haz clic en `Activar plugin`.

4. **Verificar la Instalación:**
   - Asegúrate de que el plugin **Leaflet Map** esté instalado y activado, ya que **Kobo Maps Integration** depende de él.

## Uso del Plugin

### 1. Acceder a la Gestión de Mapas

- En el panel de administración de WordPress, encontrarás una nueva sección llamada `Mapas`.
- Navega a `Mapas` > `Todos los Mapas` para ver la lista de mapas existentes.

### 2. Crear un Nuevo Mapa

1. **Añadir Nuevo Mapa:**
   - Haz clic en `Añadir Nuevo Mapa`.

2. **Paso 1: Subir Archivos**
   - **Nombre del Mapa:** Ingresa un nombre descriptivo para tu mapa.
   - **Descripción del Mapa:** Proporciona una descripción detallada del mapa.
   - **Archivo GeoJSON:** Sube tu archivo GeoJSON que contiene los datos geoespaciales.
   - **Archivo ZIP de Multimedia (Opcional):** Sube un archivo ZIP que contenga las imágenes, audios o videos asociados a los puntos del mapa.
   - Haz clic en `Guardar Mapa` para procesar los archivos.

3. **Paso 2: Configurar el Mapa**
   - **Seleccionar Campos para Popups:** Marca los campos del GeoJSON que deseas mostrar en los popups del mapa.
   - **Campo para Búsqueda:** Selecciona el campo que habilitará la función de búsqueda en el mapa.
   - **Campo para Agrupación:** Selecciona el campo que permitirá agrupar los marcadores en el mapa.
   - Haz clic en `Guardar Mapa` para finalizar la configuración.

4. **Resultado:**
   - Después de guardar, verás un mensaje de éxito con un botón para editar el mapa recién creado.
   - También se generará un shortcode que puedes copiar y pegar en cualquier página o entrada para mostrar el mapa.

### 3. Editar un Mapa Existente

1. **Seleccionar el Mapa:**
   - En `Mapas` > `Todos los Mapas`, busca el mapa que deseas editar.
   - Haz clic en el botón `Editar` correspondiente.

2. **Actualizar la Configuración:**
   - Puedes actualizar el nombre, descripción, archivos GeoJSON y ZIP.
   - Modifica los campos seleccionados para los popups, búsqueda y agrupación según sea necesario.
   - Guarda los cambios haciendo clic en `Actualizar Mapa`.

3. **Resultado:**
   - Después de actualizar, verás un mensaje de éxito con un botón para volver a editar si lo deseas.
   - El shortcode se actualizará automáticamente si has realizado cambios en la configuración.

### 4. Eliminar un Mapa

- En `Mapas` > `Todos los Mapas`, haz clic en el botón `Eliminar` correspondiente al mapa que deseas eliminar.
- Confirma la eliminación cuando se te solicite.

## Shortcodes

Una vez creado un mapa, se generará un shortcode que puedes utilizar para insertarlo en cualquier página o entrada de tu sitio.

### Cómo Usar el Shortcode

1. **Copiar el Shortcode:**
   - Después de crear o editar un mapa, copia el shortcode que se muestra en el mensaje de éxito.

2. **Insertar el Shortcode:**
   - Ve a la página o entrada donde deseas mostrar el mapa.
   - Pega el shortcode en el editor de contenido.
   - Publica o actualiza la página para ver el mapa en acción.

### Ejemplo de Shortcode

```plaintext
[leaflet-map height="600px" fitbounds scrollwheel]
[leaflet-geojson src="URL_DEL_GEOJSON" fitbounds]
<strong>Nombre:</strong> {Nombre}
<img src="{attachment_url}/Foto" alt="Foto" style="max-width:100%;">
[/leaflet-geojson]
[zoomhomemap fit]
[fullscreen position="topright"]
[leaflet-search propertyName="Nombre"]
[geojsonmarker property="Tipo_de_ataque" auto cluster-options]
```

## Personalización Avanzada

Puedes personalizar aún más tus mapas ajustando los parámetros de los shortcodes directamente en el archivo `shortcode-generator.php`. Asegúrate de entender cómo funcionan los shortcodes de Leaflet Map para realizar modificaciones avanzadas.

## Preguntas Frecuentes (FAQ)

### ¿Necesito conocimientos técnicos para usar este plugin?

No, **Kobo Maps Integration** está diseñado para ser intuitivo y fácil de usar, incluso si no tienes experiencia técnica. Sigue las instrucciones de la sección de uso para crear y gestionar tus mapas.

### ¿Qué formatos de archivo son compatibles?

- **GeoJSON:** Debes subir archivos con extensión `.geojson`.
- **Multimedia:** El archivo ZIP puede contener imágenes (`.jpg`, `.jpeg`, `.png`, `.gif`), audios (`.mp3`, `.wav`) y videos (`.mp4`, `.avi`, `.mov`).

### ¿Puedo actualizar los archivos de un mapa existente?

Sí, puedes editar un mapa existente y subir nuevos archivos GeoJSON o ZIP para actualizar su contenido.

### ¿Dónde se almacenan los archivos subidos?

Los archivos GeoJSON y ZIP se almacenan en el directorio `wp-content/uploads/kobo-maps/NOMBRE_DEL_MAPA/`.

## Soporte

Si encuentras algún problema o tienes preguntas adicionales sobre el uso del plugin, por favor contacta con el desarrollador a través de [tu correo electrónico de soporte] o abre un issue en el repositorio del plugin.

## Contribuciones

Las contribuciones son bienvenidas. Si deseas contribuir al desarrollo de **Kobo Maps Integration**, por favor sigue estos pasos:

1. **Fork del Repositorio:** Crea un fork del repositorio del plugin.
2. **Crear una Rama:** Crea una nueva rama para tu feature o bugfix.
3. **Realizar Cambios:** Implementa tus cambios y asegúrate de seguir las buenas prácticas de codificación.
4. **Pull Request:** Envía un pull request describiendo tus cambios.

## Changelog

### 2.0.0

- Segunda versión del plugin.
- Funcionalidades de creación, edición y eliminación de mapas.
- Integración con Leaflet Map.
- Se elimina requisitos de API Kobo para simplificar la gestión y sostenibilidad de los mapas
- Procesamiento de archivos GeoJSON no estandar generados por Kobotoolbox
- Generación automática de shortcodes.
- Mejoras en la visualización de los mapas
- Gestión de archivos GeoJSON y ZIP con multimedia.

### 1.0.0

- Lanzamiento inicial del plugin.
- Funcionalidades de creación, edición y eliminación de mapas.
- Generación automática de shortcodes.
- Uso API de Kobotoolbox

## Licencia

Este plugin está licenciado bajo la [Licencia GPLv2 o superior](https://www.gnu.org/licenses/gpl-2.0.html).

---

¡Gracias por elegir **Kobo Maps Integration**! Esperamos que este plugin te ayude a crear mapas interactivos impresionantes en tu sitio web.