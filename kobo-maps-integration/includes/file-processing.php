<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Salir si se accede directamente
}

// Función para procesar el archivo GeoJSON
function kmi_process_geojson_file( $file_path ) {
    $raw_data = file_get_contents( $file_path );
    $data = json_decode( $raw_data, true );

    if ( json_last_error() !== JSON_ERROR_NONE ) {
        return new WP_Error( 'invalid_json', 'Error al cargar el archivo JSON: ' . json_last_error_msg() );
    }

    // Validar y corregir la estructura
    if ( is_array( $data ) && isset( $data[0]['type'] ) ) {
        $features = array();
        foreach ( $data as $item ) {
            if ( isset( $item['features'] ) ) {
                $features = array_merge( $features, $item['features'] );
            }
        }
        $data = array(
            'type'     => 'FeatureCollection',
            'features' => $features,
        );
    } elseif ( isset( $data['type'] ) && $data['type'] === 'FeatureCollection' ) {
        // Estructura correcta
    } else {
        return new WP_Error( 'invalid_structure', 'El archivo no tiene una estructura válida para GeoJSON.' );
    }

    // Validar y corregir cada feature
    $corrected_features = array();
    foreach ( $data['features'] as $feature ) {
        if ( ! is_array( $feature ) ) {
            continue;
        }
        if ( ! isset( $feature['type'] ) || $feature['type'] !== 'Feature' ) {
            $feature['type'] = 'Feature';
        }
        if ( ! isset( $feature['geometry'] ) || ! is_array( $feature['geometry'] ) ) {
            $feature['geometry'] = array( 'type' => 'Point', 'coordinates' => array( 0, 0 ) );
        }
        if ( ! isset( $feature['properties'] ) || ! is_array( $feature['properties'] ) ) {
            $feature['properties'] = array();
        }
        $corrected_features[] = $feature;
    }

    $data['features'] = $corrected_features;

    // Guardar el archivo corregido
    $fixed_file_path = str_replace( '.geojson', '_fixed.geojson', $file_path );
    file_put_contents( $fixed_file_path, json_encode( $data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT ) );

    return $fixed_file_path;
}

// Función para procesar el archivo ZIP y extraer todos los archivos directamente en $destination
function kmi_process_zip_file( $zip_file_path, $destination ) {
    $zip = new ZipArchive;
    if ( $zip->open( $zip_file_path ) === TRUE ) {
        if ( ! file_exists( $destination ) ) {
            wp_mkdir_p( $destination );
        }

        for ( $i = 0; $i < $zip->numFiles; $i++ ) {
            $filename = $zip->getNameIndex( $i );
            $fileinfo = $zip->statIndex( $i );

            // Verificar si es un archivo (no un directorio)
            if ( substr( $filename, -1 ) !== '/' ) {
                // Obtener el contenido del archivo
                $file_content = $zip->getFromIndex( $i );
                // Obtener el nombre base del archivo
                $basename = basename( $filename );
                // Ruta completa de destino
                $destination_path = $destination . '/' . $basename;
                // Guardar el archivo en el destino
                file_put_contents( $destination_path, $file_content );
            }
        }

        $zip->close();
        return true;
    } else {
        return new WP_Error( 'zip_error', 'No se pudo abrir el archivo ZIP.' );
    }
}

// Función para organizar archivos multimedia
function kmi_organize_attachments( $source_dir, $destination_dir ) {
    if ( ! file_exists( $destination_dir ) ) {
        wp_mkdir_p( $destination_dir );
    }

    $files = glob( $source_dir . '/*' );
    foreach ( $files as $file ) {
        if ( is_file( $file ) ) {
            $file_name = basename( $file );
            // No es necesario obtener el submission_id en este caso
            // $submission_id = kmi_get_submission_id_from_filename( $file_name );

            // Eliminar el prefijo del nombre del archivo
            // $sub_dir = $destination_dir . '/' . $submission_id;

            if ( ! file_exists( $destination_dir ) ) {
                wp_mkdir_p( $destination_dir );
            }

            rename( $file, $destination_dir . '/' . $file_name );
        }
    }
}

// Función para extraer el submission_id del nombre del archivo
function kmi_get_submission_id_from_filename( $file_name ) {
    // Implementa lógica para obtener el submission_id
    // Aquí asumiremos que el submission_id es el prefijo del nombre del archivo antes del primer guión bajo
    $parts = explode( '_', $file_name );
    return $parts[0];
}
