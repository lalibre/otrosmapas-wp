<?php
/*
Plugin Name: Kobo Maps Integration
Plugin URI: https://lalibre.net/kobo-maps-integration
Description: Importa y procesa archivos GeoJSON y ZIP de KoboToolbox y muestra mapas interactivos usando Leaflet.
Version: 2.0.0-b1
Requires PHP: 7.2
Requires at least: 5.2
Tested up to: 6.4
Author: LaLibre.net Tecnologías Comunitarias
Author URI: https://lalibre.net
License: GPL2
Text Domain: kobo-maps-integration
Domain Path: /languages
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Salir si se accede directamente
}

// Definir constantes
define( 'KMI_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'KMI_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Incluir archivos necesarios
require_once KMI_PLUGIN_DIR . 'includes/admin-pages.php';
require_once KMI_PLUGIN_DIR . 'includes/db-functions.php';
require_once KMI_PLUGIN_DIR . 'includes/file-processing.php';
require_once KMI_PLUGIN_DIR . 'includes/shortcode-generator.php';

// Registrar hooks de activación y desactivación
register_activation_hook( __FILE__, 'kmi_activate_plugin' );
register_deactivation_hook( __FILE__, 'kmi_deactivate_plugin' );

function kmi_activate_plugin() {
    // Verificar dependencias
    if ( ! kmi_check_dependencies() ) {
        deactivate_plugins( plugin_basename( __FILE__ ) );
        wp_die( __( 'Este plugin requiere los plugins "extensions-leaflet-map" y "leaflet-map" para funcionar. Por favor, instálalos y actívalos.', 'kobo-maps-integration' ), __( 'Error de Dependencias', 'kobo-maps-integration' ), array( 'back_link' => true ) );
    }
    // Crear tablas en la base de datos
    kmi_create_database_table();
}

function kmi_deactivate_plugin() {
    // Opcional: realizar acciones al desactivar el plugin
}

// Verificar dependencias
function kmi_check_dependencies() {
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    return True; is_plugin_active( 'leaflet-map/leaflet-map.php' ) && is_plugin_active( 'extensions-leaflet-map/leaflet-map-extensions.php' );
}

// Inicializar el plugin
add_action( 'plugins_loaded', 'kmi_init_plugin' );

function kmi_init_plugin() {
    // Verificar dependencias
    if ( ! kmi_check_dependencies() ) {
        add_action( 'admin_notices', 'kmi_dependencies_error_notice' );
        return;
    }

    // Cargar archivos de idioma
    load_plugin_textdomain( 'kobo-maps-integration', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    // Registrar acciones y filtros necesarios
    add_action( 'admin_menu', 'kmi_add_admin_menu' );
    add_action( 'admin_enqueue_scripts', 'kmi_enqueue_admin_scripts' );

    // Registrar shortcodes
    add_shortcode( 'kobo_map', 'kmi_render_map_shortcode' );
}

function kmi_dependencies_error_notice() {
    echo '<div class="error"><p>';
    _e( 'El plugin "Kobo Maps Integration" requiere los plugins "leaflet-map" y "extensions-leaflet-map" para funcionar correctamente. Por favor, instálalos y actívalos.', 'kobo-maps-integration' );
    echo '</p></div>';
}

function kmi_enqueue_admin_scripts( $hook ) {
    // Encolar scripts y estilos para el panel de administración
    if ( strpos( $hook, 'kobo_maps' ) !== false || strpos( $hook, 'kmi_' ) !== false ) {
        wp_enqueue_style( 'kmi-admin-style', KMI_PLUGIN_URL . 'assets/css/style.css' );
        // Puedes agregar scripts aquí si es necesario
    }
}

function kmi_add_admin_menu() {
    // Añadir menú principal y submenús
    add_menu_page(
        __( 'Kobo Maps', 'kobo-maps-integration' ),
        __( 'Kobo Maps', 'kobo-maps-integration' ),
        'manage_options',
        'kmi_welcome_page',
        'kmi_welcome_page_content',
        'dashicons-location-alt'
    );

    add_submenu_page(
        'kmi_welcome_page',
        __( 'Bienvenido', 'kobo-maps-integration' ),
        __( 'Bienvenido', 'kobo-maps-integration' ),
        'manage_options',
        'kmi_welcome_page',
        'kmi_welcome_page_content'
    );

    add_submenu_page(
        'kmi_welcome_page',
        __( 'Mapas', 'kobo-maps-integration' ),
        __( 'Mapas', 'kobo-maps-integration' ),
        'manage_options',
        'kobo_maps',
        'kmi_maps_list_page'
    );

    add_submenu_page(
        'kmi_welcome_page',
        __( 'Añadir Nuevo Mapa', 'kobo-maps-integration' ),
        __( 'Añadir Nuevo Mapa', 'kobo-maps-integration' ),
        'manage_options',
        'kmi_add_new_map',
        'kmi_add_new_map_page'
    );
}

// Contenido de la página de bienvenida

function kmi_welcome_page_content() {
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
    ?>
    <div class="wrap">
        <h1><?php esc_html_e( 'Bienvenido a Kobo Maps Integration', 'kobo-maps-integration' ); ?></h1>
     
        <p><?php esc_html_e( 'Kobo Maps Integration te permite importar y procesar archivos GeoJSON y ZIP de KoboToolbox para mostrar mapas interactivos en tu sitio web utilizando Leaflet y sus extensiones.', 'kobo-maps-integration' ); ?></p>
        <p>
            <a href="<?php echo admin_url( 'admin.php?page=kmi_add_new_map' ); ?>" class="button button-primary"><?php esc_html_e( 'Añadir Nuevo Mapa', 'kobo-maps-integration' ); ?></a>
            <a href="<?php echo admin_url( 'admin.php?page=kobo_maps' ); ?>" class="button"><?php esc_html_e( 'Ver Mapas', 'kobo-maps-integration' ); ?></a>
        </p>
        <h2><?php esc_html_e( 'Requisitos', 'kobo-maps-integration' ); ?></h2>
        <ul>
            <li><?php printf( __( 'WordPress %s o superior', 'kobo-maps-integration' ), '5.0' ); ?></li>
            <li><?php printf( __( 'Plugin %s instalado y activado.', 'kobo-maps-integration' ), '<a href="https://wordpress.org/plugins/leaflet-map/" target="_blank">Leaflet Map</a>' ); ?></li>
            <li><?php printf( __( 'Plugin %s instalado y activado.', 'kobo-maps-integration' ), '<a href="https://wordpress.org/plugins/extensions-leaflet-map" target="_blank">Extensions Leaflet Map</a>' ); ?></li>
        </ul>
        
        <h2><?php esc_html_e( 'Instrucciones de Uso', 'kobo-maps-integration' ); ?></h2>
        <ol>
            <li><?php esc_html_e( 'Ve a la sección "Mapas" en el panel de administración de WordPress.', 'kobo-maps-integration' ); ?></li>
            <li><?php esc_html_e( 'Haz clic en "Añadir Nuevo Mapa".', 'kobo-maps-integration' ); ?></li>
            <li><?php esc_html_e( 'Completa el formulario con el nombre y descripción del mapa.', 'kobo-maps-integration' ); ?></li>
            <li><?php esc_html_e( 'Sube tu archivo GeoJSON y, opcionalmente, un archivo ZIP con archivos multimedia.', 'kobo-maps-integration' ); ?></li>
            <li><?php esc_html_e( 'Configura los campos para los popups, búsqueda y agrupación según tus necesidades.', 'kobo-maps-integration' ); ?></li>
            <li><?php esc_html_e( 'Guarda el mapa y copia el shortcode generado para insertarlo en tus páginas o entradas.', 'kobo-maps-integration' ); ?></li>
        </ol>
        
        <h2><?php esc_html_e( 'Repositorio y Colaboración', 'kobo-maps-integration' ); ?></h2>
        <p><?php esc_html_e( 'Kobo Maps Integration es un proyecto de código abierto y tu colaboración es bienvenida. Puedes acceder al código fuente, reportar problemas, sugerir mejoras o contribuir directamente al desarrollo a través de nuestro repositorio en GitHub.', 'kobo-maps-integration' ); ?></p>
        <p>
            <a href="https://gitlab.com/lalibre/otrosmapas-wp" target="_blank" class="button button-secondary"><?php esc_html_e( 'Visitar Repositorio en GitHub', 'kobo-maps-integration' ); ?></a>
        </p>
        <p>
            <a href="https://gitlab.com/lalibre/otrosmapas-wp" target="_blank" class="button button-primary"><?php esc_html_e( 'Contribuir Ahora', 'kobo-maps-integration' ); ?></a>
        </p>
        
    </div>
    <?php
}
