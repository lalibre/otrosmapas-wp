<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Salir si se accede directamente
}

// Función para generar el shortcode basado en los campos seleccionados, utilizando el plugin leaflet-map
function kmi_generate_leaflet_shortcode( $args ) {
    $geojson_url        = $args['geojson_url'];
    $fields             = $args['fields'];
    $search_field       = $args['search_field'];
    $group_field        = $args['group_field'];
    $map_height         = '600px'; // Mantener la altura de 600px
    $map_name           = $args['map_name']; // Asegurar que este valor esté disponible
    $geojson_file_path  = $args['geojson_file_path']; // Asegurar que este valor esté disponible

    // [leaflet-map]: se quita zoomcontrol, se mantiene fitbounds y scrollwheel
    $shortcode = '[leaflet-map height="' . esc_attr($map_height) . '" fitbounds scrollwheel]' . "\n";

    // Incluir el contenido del popup directamente en [leaflet-geojson]
    $shortcode .= '[leaflet-geojson src="' . esc_url($geojson_url) . '" fitbounds]' . "\n";

    // Generar el contenido del popup dinámicamente sin usar [popup]
    if ( ! empty($fields) ) {
        foreach ( $fields as $field ) {
            $field_lower = strtolower($field);
            if ( strpos($field_lower, 'foto') !== false || strpos($field_lower, 'imagen') !== false ) {
                $shortcode .= '<img src="{attachment_url}/{' . esc_attr($field) . '}" alt="Foto" style="max-width:100%;">' . "\n";
            } elseif ( strpos($field_lower, 'audio') !== false ) {
                $shortcode .= '<audio controls style="max-width:100%;"><source src="{attachment_url}/{' . esc_attr($field) . '}" type="audio/mpeg"></audio>' . "\n";
            } elseif ( strpos($field_lower, 'video') !== false ) {
                $shortcode .= '<video controls style="max-width:100%;"><source src="{attachment_url}/{' . esc_attr($field) . '}" type="video/mp4"></video>' . "\n";
            } else {
                $shortcode .= '<strong>' . esc_html($field) . ':</strong> {' . esc_attr($field) . '}<br>' . "\n";
            }
        }
    }

    $shortcode .= '[/leaflet-geojson]' . "\n";

    // Añadir [zoomhomemap fit]
    $shortcode .= '[zoomhomemap fit]' . "\n";

    // Mantener fullscreen
    $shortcode .= '[fullscreen position="topright"]' . "\n";

    if ( $search_field ) {
        $shortcode .= '[leaflet-search propertyName="' . esc_attr($search_field) . '"]' . "\n";
    }

    // Reemplazar [leaflet-optiongroup ...] por [geojsonmarker property="$group_field" auto cluster-options]
    if ( $group_field ) {
        $shortcode .= '[geojsonmarker property="' . esc_attr($group_field) . '" auto cluster-options]' . "\n";
    }

    // Reemplazar {attachment_url} por la URL correcta
    $upload_dir      = wp_upload_dir();
    $attachment_url  = $upload_dir['baseurl'] . '/kobo-maps/' . sanitize_file_name($map_name) . '/attachments';
    $shortcode       = str_replace('{attachment_url}', esc_url($attachment_url), $shortcode);

    return $shortcode;
}

// Función para obtener valores y grupos del campo de agrupación
function kmi_get_values_and_groups( $geojson_file_path, $group_field ) {
    if ( empty($geojson_file_path) || ! file_exists($geojson_file_path) ) {
        return array(); // Retornar un arreglo vacío si el archivo no existe
    }
    $geojson_data = json_decode(file_get_contents($geojson_file_path), true);
    $values = array();
    if ( isset($geojson_data['features']) ) {
        foreach ( $geojson_data['features'] as $feature ) {
            if ( isset($feature['properties'][$group_field]) ) {
                $value = $feature['properties'][$group_field];
                $values[$value] = $value;
            }
        }
    }
    return $values;
}
